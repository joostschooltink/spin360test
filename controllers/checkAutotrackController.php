<?php

class CheckAutotrack extends Controller
{
    protected function heeftAutotrack()
    {
        $viewmodel = new CheckAutotrackModel();
        $results = $viewmodel->heeftAutotrack();
        if ($results)
        {
            foreach ($results as $result)
            {
                if ($viewmodel->klantGegevens($result["ac"]))
                {
                    foreach ($viewmodel->klantGegevens($result["ac"]) as $klant)
                    {
                        $viewmodel->stuurMail($klant);
                    }                    
                }
                else
                {
                    exit("Geen resultaten.");
                }
            }
        }
        else
        {
            exit("Geen resultaten.");
        }
    }
}
