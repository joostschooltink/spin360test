<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Spincar Api</title>
</head>

<body>
    <div class="container">
        <div class="row">
            <?= require($view); ?>
        </div>
    </div>
</body>

</html>