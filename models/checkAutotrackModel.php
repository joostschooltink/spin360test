<?php

class CheckAutotrackModel extends Model
{
    /**
     * Kijkt of er nieuwe Autotrack aanmeldingen zijn
     */
    public function heeftAutotrack()
    {
        $this->query("SELECT * FROM klanten_zoekkeys WHERE  zoek_id = '159'");
        return $this->resultSet();
    }

    /**
     * Haalt de klant gevens op aan de hand van het AC nummer
     */
    public function klantGegevens($ac)
    {
        $this->query("SELECT bedrijf, straatnaam, huisnummer, postcode, woonplaats, contactpersoon, email
            FROM klanten
            WHERE ac = :ac");
        $this->bind(":ac", $ac);

        $this->log->lwrite("De variable bij bovenstaande query is:");
        $this->log->lwrite(":ac = " . $ac);
        $this->log->lclose();

        return $this->resultSet();
    }

    /**
     * Stuurt een mail naar carboso met de gevens van de klant
     * Stuurt een copie van de mail als CC naar support@autosoft.eu
     */
    // public function stuurMail($klanten)
    public function stuurMail($klant)
    {
        // Variablen
        $bedrijf = $klant["bedrijf"];
        $straatnaam = $klant["straatnaam"];
        $huisnummer = $klant["huisnummer"];
        $postcode = $klant["postcode"];
        $woonplaats = $klant["woonplaats"];
        $contactpersoon = $klant["contactpersoon"];
        $email = $klant["email"];
        $rootUrl = ROOT_URL;

        // Email onderwerp en body
        $onderwerp = "Nieuwe aanmelding autotrack";
        $body = <<<MESSAGE
        Beste meneer/mevrouw,

        In deze mail nieuwe aanmeldingen voor spin360 accounts.
        Het gaat om:
        Bedrijfsnaam: $bedrijf
        Straatnaam: $straatnaam
        Huisnummer: $huisnummer
        Postcode: $postcode
        Woonplaats: $woonplaats
        Contactpersoon: $contactpersoon
        Email adres: $email

        Met vriendelijke groet / With kind regards / Mit freundlichen Grüssen,

        Het Autosoft Team
        Autosoft BV
        
        
        T: 053-4280098
        M: support@autosoft.eu

        <img src='$rootUrl/assets/images/Autosoft-logo.png' alt='logo' width='300' background-color='white'>

        <small>
        De informatie opgenomen in dit bericht kan vertrouwelijk zijn en is uitsluitend bestemd voor de geadresseerde.
        Indien u dit bericht onterecht ontvangt wordt u verzocht de inhoud niet te gebruiken en de afzender direct te informeren door het bericht te retourneren. 
        Op al onze aanbiedingen en overeenkomsten zijn de NLdigital Voorwaarden 2020 van toepassing. 
        KvK: 08123151 | BTW: NL8133.58.097.B.01 | Directie: W.M.W. Koenderink | IBAN: NL38RABO0317223127 | BIC: RABONL2U
        </small>
        MESSAGE;

        // Mail header
        $headers[] = "MIME-Version: 1.0";
        $headers[] = "Content-type:text/html;charset=UTF-8";
        $headers[] = "From: " . MAIL_SENDER;
        $headers[] = "Cc: " . MAIL_TO_CC;

        // Stuur de mail
        if (mail(MAIL_TO, $onderwerp, nl2br($body), implode("\r\n", $headers)))
        {
            $this->log->lwrite("Mail is verzonden naar: " . MAIL_TO . " en als CC naar: " . MAIL_TO_CC);
            $this->log->lwrite("Onderwerp: " . $onderwerp);
            $this->log->lwrite("Body: " . $body);
            $this->log->lwrite("Afzender: " . MAIL_SENDER);
            $this->log->lclose();

            echo "Mail met succes verzonden.";
        }
        else
        {
            $this->log->lwrite("(ERROR) Er is geen mail verzonden!");
            $this->log->lclose();
            exit(error_get_last()['message']);
        }
    }
}
