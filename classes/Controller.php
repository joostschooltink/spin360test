<?php
abstract class Controller
{
    protected $request, $action;
    public $log;

    public function __construct($action, $request)
    {
        // Maak een nieuwe instantie van de Logging class
        $this->log = new Logging();
        // Path naar logfile
        $this->log->lfile(LOG_PATH);

        $this->action = $action;
        $this->request = $request;
    }

    public function executeAction()
    {
        return $this->{$this->action}();
    }

    protected function returnView($viewmodel, $fullview)
    {
        $view = 'views/' . strtolower(get_class($this)) . '/' . $this->action . '.php';
        if ($fullview)
        {
            require('views/main.php');
        }
        else
        {
            require($view);
        }
    }
}
