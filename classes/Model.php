<?php
abstract class Model
{
    protected $dbh, $stmt;
    public $log;

    public function __construct()
    {
        // Maak een nieuwe instantie van de Logging class
        $this->log = new Logging();
        // Path naar logfile
        $this->log->lfile(LOG_PATH);
        try
        {
            $this->dbh = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASS, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        }
        catch (PDOException $e)
        {
            $this->log->lwrite("Connectie met de database mislukt: \n" . $e->getMessage());
            $this->log->lclose();
            exit;
        }
    }

    public function query($query)
    {
        try
        {
            $this->stmt = $this->dbh->prepare($query);

            $this->log->lwrite($query);
            $this->log->lclose();
        }
        catch (Exception $e)
        {
            $this->log->lwrite("Er is wat mis gegaan tijdens de prepare statement: \n" . $e->getMessage());
            $this->log->lclose();
            exit;
        }
    }

    //Binds the prep statement
    public function bind($param, $value, $type = null)
    {
        try
        {
            if (is_null($type))
            {
                switch (true)
                {
                    case is_int($value):
                        $type = PDO::PARAM_INT;
                        break;
                    case is_bool($value):
                        $type = PDO::PARAM_BOOL;
                        break;
                    case is_null($value):
                        $type = PDO::PARAM_NULL;
                        break;
                    default:
                        $type = PDO::PARAM_STR;
                }
            }
            $this->stmt->bindValue($param, $value, $type);
        }
        catch (Exception $e)
        {
            $this->log->lwrite("Er is wat mis gegaan tijdens de bind statement: \n" . $e->getMessage());
            $this->log->lclose();
            exit;
        }
    }

    public function execute()
    {
        try
        {
            $this->stmt->execute();
        }
        catch (Exception $e)
        {
            $this->log->lwrite("Er is wat mis gegaan tijdens de execute statement: \n" . $e->getMessage());
            $this->log->lclose();
            exit;
        }
    }

    public function resultSet()
    {
        try
        {
            $this->execute();
            return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        catch (Exception $e)
        {
            $this->log->lwrite("Er is wat mis gegaan tijdens het ophalen van de resultSet: \n" . $e->getMessage());
            $this->log->lclose();
            exit;
        }
    }

    public function lastInsertId()
    {
        try
        {
            return $this->dbh->lastInsertId();
        }
        catch (Exception $e)
        {
            $this->log->lwrite("Er is wat mis gegaan bij het ophalen van laatst ingevoerde ID: \n" . $e->getMessage());
            $this->log->lclose();
            exit;
        }
    }

    public function single()
    {
        try
        {
            $this->execute();
            return $this->stmt->fetch(PDO::FETCH_ASSOC);
        }
        catch (Exception $e)
        {
            $this->log->lwrite("Er is wat mis gegaan bij het ophalen van een single result: \n" . $e->getMessage());
            $this->log->lclose();
            exit;
        }
    }
}